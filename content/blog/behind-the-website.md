---
title: "Behind this website"
date: 2019-01-20
tags: [ "gitlab","bts","devops" ]
---

# Behind the scenes ...

## Choices
This site aims to be a showcase site with a small blog to post articles. Such a website must load as fast as possible without memory consumption because served page is not dependent of user input !    
That is why I looked into website compiled once and servedas static, and Hugo is a very light-weight framework to build showcase site.     



## Deployment
In order to keep up to date our stagging environment, a gitlab-ci workflow is the solution.
With DevOps integration, adding content is very easy (as a dynamic website but without delay drawback)


### Jobs

#### Build Hugo



#### Deploy through FTP

steps:
- login to the selected workspace
- push by replacing with new content

### Gitlab-ci.yml

```yaml
stages:
    - build
    - deploy

# Build with our template
hugo:
    image: jguyomard/hugo-builder
    stage: build
    script:
        - hugo
        - ls
    # keep the output for the next stage
    artifacts:
        paths:
        - public

# Let's deploy through FTP
ftp:
    image: ubuntu:18.04
    stage: deploy
    before_script:
        - apt-get update -qy
        - apt-get install -y lftp
    script:
        # Sync to FTP
        - lftp -e "open $FTP_SITE; user $FTP_USERNAME $FTP_PASSWORD; mirror -X .* -X .*/ --reverse --verbose --delete public/ /www/guillaume/; bye"
    dependencies: 
        - hugo


```